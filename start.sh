#!/bin/bash

set -eu

mkdir -p /run/apache2 /run/cron /run/app/sessions

### Check if a directory does not exist ###
if [ ! -d "/app/data/gnu-social" ] 
then
    echo "Directory /app/data/gnu-social DOES NOT exists." 
    echo "Pull gnu-social, and move it"
    git clone https://notabug.org/diogo/gnu-social gnu-social
    cd /app/data/gnu-social
    find . -maxdepth 1 -exec mv {} .. \;
    cd /app/data
    composer install
    mkdir -p /app/data/file
    cp /app/code/htaccess.sample /app/data/public/.htaccess
    php /app/data/scripts/install_cli.php --server="${CLOUDRON_APP_DOMAIN}" \
                                --sitename="GNU Social" \
                                --dbtype='pgsql' \
                                --host="${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}" \
                                --database="${CLOUDRON_POSTGRESQL_DATABASE}" \
                                --username="${CLOUDRON_POSTGRESQL_USERNAME}" \
                                --password="${CLOUDRON_POSTGRESQL_PASSWORD}" \
                                --admin-nick="socialadmin" \
                                --admin-pass="changeme123" \
                                --fancy=yes \
                                --verbose
    
    echo -e "#!/bin/bash\n\n# Place custom startup commands here" > /app/data/run.sh
    
    if [[ -f "/app/data/config.php" ]]; then
        ## Add stuff to the config.php for mailer, plugins, etc.
        echo "\$config['db']['schemacheck'] = 'script';" >> /app/data/config.php
        echo "\$config['mail']['domain'] = 'GNU Social';" >> /app/data/config.php
        echo "\$config['mail']['notifyfrom'] = '\"GNU Social\" <${CLOUDRON_MAIL_FROM}>';" >> /app/data/config.php
        echo "\$config['mail']['backend'] = 'smtp';" >> /app/data/config.php
        echo "\$config['mail']['debug'] = false;" >> /app/data/config.php
        echo "\$config['mail']['params'] = array(" >> /app/data/config.php
        echo "'host' => '${CLOUDRON_MAIL_SMTP_SERVER}'," >> /app/data/config.php
        echo "'port' => ${CLOUDRON_MAIL_SMTP_PORT}," >> /app/data/config.php
        echo "'auth' => true," >> /app/data/config.php
        echo "'username' => '${CLOUDRON_MAIL_SMTP_USERNAME}'," >> /app/data/config.php
        echo "'password' => '${CLOUDRON_MAIL_SMTP_PASSWORD}'" >> /app/data/config.php
        echo ");" >> /app/data/config.php
        echo "" >> /app/data/config.php
        echo "\$config['queue']['enabled'] = true;" >> /app/data/config.php
        echo "\$config['queue']['daemon'] = true;" >> /app/data/config.php
        echo "\$config['queue']['subsystem'] = 'redis';" >> /app/data/config.php
        echo "\$config['ostatus']['hub_retries'] = 3;" >> /app/data/config.php
        echo "" >> /app/data/config.php
        echo "\$config['profile']['delete'] = true;" >> /app/data/config.php
        echo "\$config['thumbnail']['maxsize'] = 3000;" >> /app/data/config.php
        echo "\$config['public']['localonly'] = true;" >> /app/data/config.php
        echo "" >> /app/data/config.php
        echo "" >> /app/data/config.php
        ## LDAP Configuration - GS Plugin for this is currently broken
        ## So this will be re-enabled later.
        ## echo "addPlugin('ldapAuthentication', array(" >> /app/data/config.php
        ## echo "'provider_name'=>'Cloudron'," >> /app/data/config.php
        ## echo "'authoritative'=>true," >> /app/data/config.php
        ## echo "'autoregistration'=>true," >> /app/data/config.php
        ## echo "'binddn'=>'${CLOUDRON_LDAP_BIND_DN}'," >> /app/data/config.php
        ## echo "'bindpw'=>'${CLOUDRON_LDAP_BIND_PASSWORD}'," >> /app/data/config.php
        ## echo "'basedn'=>'${CLOUDRON_LDAP_USERS_BASE_DN}'," >> /app/data/config.php
        ## echo "'host'=>array('${CLOUDRON_LDAP_SERVER}')," >> /app/data/config.php
        ## echo "'password_changeable'=>false," >> /app/data/config.php
        ## echo "'email_changeable'=>false," >> /app/data/config.php
        ## echo "'attributes'=>array(" >> /app/data/config.php
        ## echo "    'username'=>'username'," >> /app/data/config.php
        ## echo "    'nickname'=>'username'," >> /app/data/config.php
        ## echo "    'email'=>'mail'," >> /app/data/config.php
        ## echo "    'fullname'=>'displayName')" >> /app/data/config.php
        ## echo "));" >> /app/data/config.php
        ## echo "" >> /app/data/config.php        
        ## Enable certain plugins to do stuff.
        echo "addPlugin('RedisQueue', ['server' => '${CLOUDRON_REDIS_URL}']);" >> /app/data/config.php
        echo "addPlugin('StoreRemoteMedia');" >> /app/data/config.php
        
        ## Add stuff to run.sh so we do things on each reboot/restart.
        chmod +x /app/data/scripts/startdaemons.sh
        echo "php /app/data/scripts/checkschema.php" >> /app/data/run.sh
        echo "/app/data/scripts/startdaemons.sh &" >> /app/data/run.sh
      
    fi
fi


if [[ ! -f "/app/data/php.ini" ]]; then
    echo "==> Generating php.ini"
    cp /etc/php/7.3/apache2/php.ini.orig /app/data/php.ini
else
    crudini --set /app/data/php.ini Session session.gc_probability 1
    crudini --set /app/data/php.ini Session session.gc_divisor 100
fi

# source it so that env vars are persisted
echo "==> Source custom startup script"
[[ -f /app/data/run.sh ]] && source /app/data/run.sh

[[ ! -f /app/data/crontab ]] && cp /app/code/crontab.template /app/data/crontab

## configure in-container Crontab
# http://www.gsp.com/cgi-bin/man.cgi?section=5&topic=crontab
if ! (env; cat /app/data/crontab; echo -e '\nMAILTO=""') | crontab -u www-data -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

# phpMyAdmin auth file
if [[ ! -f /app/data/.phpmyadminauth ]]; then
    echo "==> Generating phpMyAdmin authentication file"
    PASSWORD=`pwgen -1 16`
    htpasswd -cb /app/data/.phpmyadminauth admin "${PASSWORD}"
    sed -e "s,PASSWORD,${PASSWORD}," /app/code/phpmyadmin_login.template > /app/data/phpmyadmin_login.txt
fi

echo "==> Creating credentials.txt"
sed -e "s,MYSQL_HOST,${CLOUDRON_MYSQL_HOST}," \
    -e "s,MYSQL_PORT,${CLOUDRON_MYSQL_PORT}," \
    -e "s,MYSQL_USERNAME,${CLOUDRON_MYSQL_USERNAME}," \
    -e "s,MYSQL_PASSWORD,${CLOUDRON_MYSQL_PASSWORD}," \
    -e "s,MYSQL_DATABASE,${CLOUDRON_MYSQL_DATABASE}," \
    -e "s,MYSQL_URL,${CLOUDRON_MYSQL_URL}," \
    -e "s,POSTGRESQL_HOST,${CLOUDRON_POSTGRESQL_HOST}," \
    -e "s,POSTGRESQL_PORT,${CLOUDRON_POSTGRESQL_PORT}," \
    -e "s,POSTGRESQL_USERNAME,${CLOUDRON_POSTGRESQL_USERNAME}," \
    -e "s,POSTGRESQL_PASSWORD,${CLOUDRON_POSTGRESQL_PASSWORD}," \
    -e "s,POSTGRESQL_DATABASE,${CLOUDRON_POSTGRESQL_DATABASE}," \
    -e "s,POSTGRESQL_URL,${CLOUDRON_POSTGRESQL_URL}," \
    -e "s,MAIL_SMTP_SERVER,${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s,MAIL_SMTP_PORT,${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,MAIL_SMTPS_PORT,${CLOUDRON_MAIL_SMTPS_PORT}," \
    -e "s,MAIL_SMTP_USERNAME,${CLOUDRON_MAIL_SMTP_USERNAME}," \
    -e "s,MAIL_SMTP_PASSWORD,${CLOUDRON_MAIL_SMTP_PASSWORD}," \
    -e "s,MAIL_FROM,${CLOUDRON_MAIL_FROM}," \
    -e "s,MAIL_DOMAIN,${CLOUDRON_MAIL_DOMAIN}," \
    -e "s,REDIS_HOST,${CLOUDRON_REDIS_HOST}," \
    -e "s,REDIS_PORT,${CLOUDRON_REDIS_PORT}," \
    -e "s,REDIS_PASSWORD,${CLOUDRON_REDIS_PASSWORD}," \
    -e "s,REDIS_URL,${CLOUDRON_REDIS_URL}," \
    /app/code/credentials.template > /app/data/credentials.txt

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

echo "==> Starting Lamp stack"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp
