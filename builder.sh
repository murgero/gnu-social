#!/bin/bash

## Build app
docker build -t mitchellurgero/org.urgero.gs:latest . --no-cache 
docker push mitchellurgero/org.urgero.gs:latest

## Install app
cloudron install --image=mitchellurgero/org.urgero.gs:latest
