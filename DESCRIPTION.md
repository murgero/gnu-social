# GNU social 2.0.x

(c) 2010-2019 Free Software Foundation, Inc

## About

GNU social is a free social networking platform. It helps people in a community, company or group to exchange short status updates, do polls, announce events, or other social activities (and you can add more!). Users can choose which people to "follow" and receive only their friends' or colleagues' status messages. It provides a similar service to proprietary social network sites, but is much more awesome.

