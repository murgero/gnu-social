THIS APP WILL TAKE A LONG TIME TO START ON THE FIRST ROUND AS IT INSTALLS GNU SOCIAL
The code and credential files can be accessed via [SFTP](https://cloudron.io/documentation/apps/#ftp-access) or the [Web Terminal](https://cloudron.io/documentation/apps/#web-terminal).

The admin login is `socialadmin` with password `changeme123`. This should be changed ASAP!
